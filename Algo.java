import java.util.ArrayList;

public class Algo {
	
	public Algo() {
		
	}
	
	public int plusCourtChemin(ArrayList<Mot> tab,Mot mot) {
		int[][] tabLong = new int[tab.size()][tab.size()];
		//On initialise toutes les longueurs des chemins à la valeur maximum possible
		for(int i=0;i<tabLong.length;i++) {
			for(int j=0;j<tabLong[i].length;j++) {
				tabLong[i][j]=Integer.MAX_VALUE;
			}
		}
		int indice = tab.indexOf(mot);
		/*for(int i=0;i<tabLong.length;i++) {
			for(int j=0;j<tabLong[i].length;j++) {
				
			}
		}*/
		for(int i=0;i<mot.getMotsVoisins().size();i++) {
			mot.getMotsVoisins().get(i);
		}
	}

	public ArrayList<ArrayList<Mot>> composantesConnexes(ArrayList<Mot> mots){
		ArrayList<Mot> colorer = new ArrayList<Mot>();
		ArrayList<ArrayList<Mot>> allCompConnexes = new ArrayList<ArrayList<Mot>>();
		for(int i=0;i<mots.size();i++){
			if(colorer.contains(mots.get(i))==false){
				ArrayList<Mot> compConnexe = new ArrayList<Mot>();
				ArrayList<Mot> aTraiter = new ArrayList<Mot>();
				aTraiter.add(mots.get(i));
				colorer.add(mots.get(i));
				while (aTraiter.isEmpty()==false){
					Mot m=aTraiter.get(0);
					aTraiter.remove(0);
					compConnexe.add(m);
					for(int j=0;j<m.getMotsVoisins().size();j++){
						if(colorer.contains(m.getMotsVoisins().get(j))==false){
							aTraiter.add(m.getMotsVoisins().get(j));
							colorer.add(m.getMotsVoisins().get(j));
						}
					}
				}
			allCompConnexes.add(compConnexe);
			}
		}
	return allCompConnexes;
	}

	public int getNbComposantesConnexes(ArrayList<Mot> mots){
		return composantesConnexes(mots).size();
	}


}
