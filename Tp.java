import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Tp {
	static ArrayList<ArrayList<Mot>> tabG =null;
	
	public static void read() throws IOException {
		try {
			
			ArrayList<ArrayList<Mot>> tabTab = new ArrayList<>();
			String[] nomsFichiers = {"mots04lettres.txt", "mots05lettres.txt", "mots06lettres.txt", "mots07lettres.txt","mots08lettres.txt",
					"mots09lettres.txt", "mots10lettres.txt", "mots11lettres.txt", "mots12lettres.txt", "mots13lettres.txt", "mots14lettres.txt", 
					"mots15lettres.txt", "mots16lettres.txt", "mots17lettres.txt", "mots18lettres.txt"};
			for(int i=0;i<nomsFichiers.length;i++) {
				ArrayList<Mot> tab = new ArrayList<>();
				String path = "/home/luca/Documents/GRAPHE_ET_APPLI/archive/";
				//String path = "C:/Users/yohan/IdeaProjects/GrapheDoublets/ressources/";
				path += nomsFichiers[i];
				InputStream flux = new FileInputStream(path);
				InputStreamReader lecture= new InputStreamReader(flux);
				BufferedReader buff = new BufferedReader(lecture);
				String ligne;
				while((ligne=buff.readLine())!=null) {
					tab.add(new Mot(ligne));
				}
				tabTab.add(tab);
			}
			tabG =tabTab;
		}
		 catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void initialiserVoisins(ArrayList<Mot> mots) {
		for(int i=0;i<mots.size();i++) {
			mots.get(i).setVoisins(mots);
		}
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		read();
		ArrayList<Mot> tab4 = tabG.get(0);
		initialiserVoisins(tab4);

		int cpt = 0;
		int cptSansVoisin=0;
		int cptUnVoisin=0;
		int degreMax=0;

		for(int i=0;i<tab4.size();i++) {
			if(tab4.get(i).getMotsVoisins().size()==0) {
				cptSansVoisin++;
			}
			if(tab4.get(i).getMotsVoisins().size()==1) {
				Mot voisin = tab4.get(i).getMotsVoisins().get(0);
				if(voisin.getMotsVoisins().size()==1) {
					cptUnVoisin++;
				}
			}

			for(int j=0;j<tab4.get(i).getMotsVoisins().size();j++) {
				cpt++;
			}
			if(tab4.get(i).getMotsVoisins().size()>degreMax){
				degreMax=tab4.get(i).getMotsVoisins().size();
			}
		}
		System.out.println("-----Graphes et Applications - Projet Doublets-----");
		System.out.println("Polytech 3A Informatique");
		System.out.println("MALBRANQUE Yohan & Savio Luca");

		System.out.println("\n----------QUESTION 1----------");
		System.out.println("Les 15 fichiers sont importés, mais nous travaillons et testons nos" +
				"\nalgorithmes sur le fichier composé de mots de 4 lettres pour des raisons de" +
				"\nsimplicité et de temps de calcul");

		System.out.println("\n----------QUESTION 2----------");
		System.out.println("Le nombre de sommets du graphe est " + tab4.size());
		System.out.println("Le nombre d'arrêtes est " + cpt/2);

		System.out.println("\n----------QUESTION 3----------");
		Algo a= new Algo();
		System.out.println("Le nombre de composantes connexes du graphe est " + a.getNbComposantesConnexes(tab4));

		System.out.println("\n----------QUESTION 4----------");
		System.out.println("Le nombre de mots sans voisin est " + cptSansVoisin);

		System.out.println("\n----------QUESTION 5----------");
		System.out.println("Le nombre de composantes composés uniquement de deux mots est " + cptUnVoisin/2);

		System.out.println("\n----------QUESTION 6----------");
		System.out.println("Le degré maximum Nmax du graphe est " + degreMax);

		for(int d=1;d<=degreMax;d++){
			int cptParDegre=0;
			for(int i=0;i<tab4.size();i++){
				if(tab4.get(i).getMotsVoisins().size()==d){
					cptParDegre++;
				}
			}
			System.out.println("Il y a " + cptParDegre + " sommets avec " + d + " voisins");
		}

		System.out.println("\n----------QUESTION 7----------");
		System.out.println("Le diamètre du graphe est");
	}
}
