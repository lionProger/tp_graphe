import java.util.ArrayList;

public class Mot {
    private String texte;
    private ArrayList<Mot> motsVoisins;

    public Mot(String texte){
        this.texte=texte;
        this.motsVoisins=new ArrayList<Mot>();
    }

    public String getTexte(){
        return this.texte;
    }

    public ArrayList<Mot> getMotsVoisins() {
        return this.motsVoisins;
    }

    public int comparerMots(Mot mot1, Mot mot2){
        int diff=0;
        for(int i=0;i<mot1.texte.length();i++){
            if(mot1.texte.charAt(i)!=mot2.texte.charAt(i)){
                diff++;
            }
        }
        return diff;
    }


    public void setVoisins (ArrayList<Mot> liste){
        ArrayList<Mot> voisins=new ArrayList<Mot>();
        for(int i=0;i<liste.size();i++){
            if(comparerMots(this,liste.get(i))==1){
                voisins.add(liste.get(i));
            }
        }
        this.motsVoisins=voisins;
    }
}
